# html_logo_url
This form of the doc attribute lets you control the logo in the upper left hand side of the docs.

`#![doc(html_logo_url = "https://example.com/logo.jpg")]` for url link<br>
or<br>
`#![doc(html_logo_url = "crown.svg")]` for local link<br>
> When using local link, the logo should be placed in ./target/doc/$RUST_APPLICATION directory. <br>
> For guessing_game application using `cargo doc --all --verbose --target-dir public`, a crown.svg logo should be placed in ./public/doc/guessing_game/crown.svg<br>
---
This will put `<a href='index.html'><img src='{}' alt='logo' width='100'></a>` into your docs, where the string for the attribute goes into the {}.

If you don't use this attribute, there will be no logo.
