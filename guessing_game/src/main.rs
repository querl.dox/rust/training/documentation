#![doc(html_favicon_url = "favicon.ico")]
#![doc(html_logo_url = "crown.svg")]


/// html_favicon_url
/// This form of the doc attribute lets you control the favicon of your docs.
/// #![doc(html_favicon_url = "https://example.com/favicon.ico")]
/// This will put <link rel="shortcut icon" href="{}"> into your docs, where the string for the attribute goes into the {}.
/// If you don't use this attribute, there will be no favicon.

/// html_logo_url
/// This form of the doc attribute lets you control the logo in the upper left hand side of the docs.
/// #![doc(html_logo_url = "https://example.com/logo.jpg")]
/// This will put <a href='index.html'><img src='{}' alt='logo' width='100'></a> into your docs, where the string for the attribute goes into the {}.
/// If you don't use this attribute, there will be no logo.

use std::io;

pub fn main () {
    /// Guess the number.
    println!("Guess the number!");

    /// input guess
    println!("Please input your guess.");

    let mut guess = String::new();

    io::stdin().read_line(&mut guess)
        .expect("Failed to read line");

    println!("You guessed: {}", guess);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn guess_88() {
        let guess = 88;
        assert_eq!(main, 87);
    }
}
