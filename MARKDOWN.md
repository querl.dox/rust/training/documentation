# Using standalone Markdown files

`rustdoc` can also generate HTML from standalone Markdown files. Let's
give it a try: create a `README.md` file with these contents:

````text
# Docs

This is a project to test out `rustdoc`.

[Here is a link!](https://www.rust-lang.org)

## Subheading

```rust
fn foo() -> i32 {
    1 + 1
}
```
````

And call `rustdoc` on it:

```bash
$ rustdoc README.md
```

You'll find an HTML file in `docs/doc/README.html` generated from its
Markdown contents.

Cargo currently does not understand standalone Markdown files, unfortunately.
