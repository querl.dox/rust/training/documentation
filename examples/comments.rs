#![doc(html_logo_url = "crown.svg", html_favicon_url = "favicon.ico")]

/// Derive the `fmt::Debug` implementation for `Structure`. `Structure`
/// is a structure which contains a single `i32`.
#[derive(Debug)]
struct Structure(i32);

/// Put a `Structure` inside of the structure `Deep`. Make it printable
/// also.
#[derive(Debug)]
struct Deep(Structure);

/// Adds one to the number given.
///
/// # Examples
///
/// ```
/// let arg = 5;
/// let answer = my_crate::add_one(arg);
///
/// assert_eq!(6, answer);
/// ```
///
/// # Panics
/// will panic
///
/// # Errors
/// this is an error
///
/// # Safety
/// what is safe
pub fn debut() {
    /// Printing with `{:?}` is similar to with `{}`.
    println!("{:?} months in a year.", 12);
    println!("{1:?} {0:?} is the {actor:?} name.",
             "Slater",
             "Christian",
             actor="actor's");

    /// `Structure` is printable!
    println!("Now {:?} will print!", Structure(3));
    
    /// The problem with `derive` is there is no control over how
    /// the results look. What if I want this to just show a `7`?
    println!("Now {:?} will print!", Deep(Structure(7)));
}

/// A human being is represented here
pub struct Person {
    /// A person must have a name, no matter how much Juliet may hate it
    name: String,
}

impl Person {
    /// Returns a person with the name given them
    ///
    /// # Arguments
    ///
    /// * `name` - A string slice that holds the name of the person
    ///
    /// # Example
    ///
    /// ```
    /// // You can have rust code between fences inside the comments
    /// // If you pass --test to Rustdoc, it will even test it for you!
    /// use doc::Person;
    /// let person = Person::new("name");
    /// ```
    pub fn new(name: &str) -> Person {
        Person {
            name: name.to_string(),
        }
    }

    /// Gives a friendly hello!
    ///
    /// Says "Hello, [name]" to the `Person` it is called on.
    pub fn hello(& self) {
        println!("Hello, {}!", self.name);
    }
}
